package com.media.api.uploader.exception.error

import com.fasterxml.jackson.annotation.JsonInclude

@JsonInclude(JsonInclude.Include.NON_NULL)
class ErrorDTO {
    private val errorMessage = ""
}