package com.media.api.uploader.service.image

import com.media.api.uploader.controller.upload.adapter.dto.UploadDTO
import com.media.api.uploader.repository.image.entity.Image
import org.springframework.web.multipart.MultipartFile

interface ImageService {
    fun saveImage(file: MultipartFile)

    fun findAll(): List<Image>

    fun deleteAll()
}