package com.media.api.uploader.service.image.impl

import com.media.api.uploader.repository.image.ImageRepository
import com.media.api.uploader.repository.image.entity.Image
import com.media.api.uploader.service.image.ImageService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import javax.sql.rowset.serial.SerialBlob

@Service("imageService")
class ImageServiceImpl : ImageService {
    @Autowired
    private lateinit var imageRepository: ImageRepository

    override fun saveImage(file: MultipartFile) {
        val image = Image()

        image.data = SerialBlob(file.bytes)

        imageRepository.save(image)

    }

    override fun findAll(): List<Image> {
        return imageRepository.findAll()
    }

    override fun deleteAll() {
        return imageRepository.deleteAll()
    }


}