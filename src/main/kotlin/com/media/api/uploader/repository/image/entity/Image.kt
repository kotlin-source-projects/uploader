package com.media.api.uploader.repository.image.entity

import java.io.Serializable
import java.sql.Blob
import javax.persistence.*

@Entity
@Table(name = "IMAGE_UPLOAD")
class Image: Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    var id: Long? = null

    var data: Blob? = null
}