package com.media.api.uploader.repository.image

import com.media.api.uploader.repository.image.entity.Image
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ImageRepository: JpaRepository<Image, Int> {
}