package com.media.api.uploader.controller.upload.impl

import com.media.api.uploader.controller.upload.UploadFileController
import com.media.api.uploader.controller.upload.adapter.dto.UploadDTO
import com.media.api.uploader.repository.image.entity.Image
import com.media.api.uploader.service.image.ImageService
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.util.StreamUtils
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartFile
import java.io.ByteArrayInputStream
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream
import javax.servlet.http.HttpServletResponse


@Tag(name = "Upload")
@RestController("uploadFileController")
@CrossOrigin(origins = ["*"], allowedHeaders = ["*"], maxAge = 3600)
@RequestMapping(value = ["/uploader-api/v1"])
class UploadFileControllerImpl : UploadFileController {

    @Autowired
    private lateinit var imageService: ImageService

    override fun uploadFile(internal: String, external: String, file: MultipartFile): ResponseEntity<UploadDTO> {

        println(internal)
        println(external)
        println("Size: " + file.size)

        imageService.saveImage(file)

        val upload = UploadDTO()
        upload.message = "OK"

        return ResponseEntity.ok().body(upload)
    }

    override fun downloadFile(response: HttpServletResponse) {
        response.contentType = "application/octet-stream"
        response.setHeader("Content-Disposition", "attachment;filename=download.zip")
        response.status = HttpServletResponse.SC_OK
        val fileNames: List<Image> = imageService.findAll()

        ZipOutputStream(response.outputStream).use { zippedOut ->
            for (file in fileNames) {
                val e = ZipEntry(file.id.toString() + ".jpg")
                e.size = if (file.data != null) file.data!!.length() else 0
                e.time = System.currentTimeMillis()
                zippedOut.putNextEntry(e)
                StreamUtils.copy(ByteArrayInputStream(file.data!!.binaryStream.readBytes()), zippedOut)
                zippedOut.closeEntry()
            }
            zippedOut.finish()
        }
    }

    override fun listAllFiles(): ResponseEntity<List<Long>> {
        val fileNames: List<Image> = imageService.findAll()
        val idList = fileNames.mapNotNull { file -> file.id }

        return ResponseEntity.ok().body(idList)
    }

    override fun deleteAllFiles(): ResponseEntity<UploadDTO> {
        imageService.deleteAll()

        val upload = UploadDTO()
        upload.message = "OK"

        return ResponseEntity.ok().body(upload)
    }
}