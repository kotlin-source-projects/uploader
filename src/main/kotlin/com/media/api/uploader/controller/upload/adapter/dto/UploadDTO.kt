package com.media.api.uploader.controller.upload.adapter.dto

import com.fasterxml.jackson.annotation.JsonInclude

@JsonInclude(JsonInclude.Include.NON_NULL)
class UploadDTO {
    var message: String

    constructor() {
        this.message = ""
    }

    constructor(message: String) {
        this.message = message
    }

}