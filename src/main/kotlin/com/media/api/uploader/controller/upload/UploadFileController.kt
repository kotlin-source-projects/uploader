package com.media.api.uploader.controller.upload

import com.media.api.uploader.controller.upload.adapter.dto.UploadDTO
import com.media.api.uploader.exception.error.ErrorDTO
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import javax.servlet.http.HttpServletResponse

interface UploadFileController {
    @PostMapping("/upload")
    @ResponseStatus(HttpStatus.ACCEPTED)
    @ApiResponses(
        value = [
            ApiResponse(
                responseCode = "200",
                description = "Returns information about upload process",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = UploadDTO::class))]
            ),
            ApiResponse(
                responseCode = "400",
                description = "Bad request",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorDTO::class))]
            ),
            ApiResponse(
                responseCode = "403",
                description = "Forbidden",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorDTO::class))]
            ),
            ApiResponse(
                responseCode = "404",
                description = "Not found",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorDTO::class))]
            ),
            ApiResponse(
                responseCode = "500",
                description = "Internal server error",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorDTO::class))]
            )]
    )
    @Operation(summary = "Upload files")
    fun uploadFile(@RequestParam("internal") internal: String, @RequestParam("external") external: String, @RequestParam(value = "file", required = true) file: MultipartFile): ResponseEntity<UploadDTO>

    @GetMapping("/download")
    @ResponseStatus(HttpStatus.ACCEPTED)
    @ApiResponses(
        value = [
            ApiResponse(responseCode = "200", description = "Returns information about dowload process"),
            ApiResponse(
                responseCode = "400",
                description = "Bad request",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorDTO::class))]
            ),
            ApiResponse(
                responseCode = "403",
                description = "Forbidden",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorDTO::class))]
            ),
            ApiResponse(
                responseCode = "404",
                description = "Not found",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorDTO::class))]
            ),
            ApiResponse(
                responseCode = "500",
                description = "Internal server error",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorDTO::class))]
            )]
    )
    @Operation(summary = "Download files in zip")
    fun downloadFile(response: HttpServletResponse)

    @GetMapping("/files")
    @ResponseStatus(HttpStatus.ACCEPTED)
    @ApiResponses(
        value = [
            ApiResponse(
                responseCode = "200",
                description = "Returns information about list process",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = UploadDTO::class))]
            ),
            ApiResponse(
                responseCode = "400",
                description = "Bad request",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorDTO::class))]
            ),
            ApiResponse(
                responseCode = "403",
                description = "Forbidden",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorDTO::class))]
            ),
            ApiResponse(
                responseCode = "404",
                description = "Not found",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorDTO::class))]
            ),
            ApiResponse(
                responseCode = "500",
                description = "Internal server error",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorDTO::class))]
            )]
    )
    @Operation(summary = "List files")
    fun listAllFiles(): ResponseEntity<List<Long>>

    @DeleteMapping("/files")
    @ResponseStatus(HttpStatus.ACCEPTED)
    @ApiResponses(
        value = [
            ApiResponse(
                responseCode = "200",
                description = "Returns information about delete process",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = UploadDTO::class))]
            ),
            ApiResponse(
                responseCode = "400",
                description = "Bad request",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorDTO::class))]
            ),
            ApiResponse(
                responseCode = "403",
                description = "Forbidden",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorDTO::class))]
            ),
            ApiResponse(
                responseCode = "404",
                description = "Not found",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorDTO::class))]
            ),
            ApiResponse(
                responseCode = "500",
                description = "Internal server error",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorDTO::class))]
            )]
    )
    @Operation(summary = "Delete files")
    fun deleteAllFiles(): ResponseEntity<UploadDTO>
}
